Applications Under Development:

scl_benchmarks :
Can simulate the dynamics of numerous robot models in 
trunk/specs/Benchmarks/*xml.

scl_file_converter :
Converts sai and opensim file formats into native xml

scl_gc_ctrl :
Generic generalized coordinate controller (Eg. joint space)
Files to use : trunk/specs/*Cfg.xml

scl_lib :
Creates a dynamically linked library for the remaining scl 
applications to use.

scl_robot_creator : (under dev)
A sample application to enable users to graphically build
a robot model.

scl_skeleton_code :
A sample application that anyone can modify to build their
own controller

scl_task_ctrl :
Generic operational space controller (Eg. hand controller)
Files to use : trunk/specs/*Cfg.xml

scl_test : 
A test suite for scl functionality. A good place to
start for exploring the scl library's functions.
