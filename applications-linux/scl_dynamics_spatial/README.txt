This is an example app to test the spatial dynamics integrator
implementation in scl.

1. Compile with the CMake system
$ sh make_dbg.sh
or 
$ sh make_rel.sh

2. Run
$ ./scl_spatial_integrator

Use the mouse to move the camere around:
rotate camera : l-click + drag
zoom camera : r-click + drag
move camera : l-click+ctrl+drag
